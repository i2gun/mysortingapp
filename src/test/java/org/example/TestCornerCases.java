package org.example;

import org.junit.Test;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class TestCornerCases {

    Main sorting = new Main();

    @Test
    public void testElevenElementArrayCase() {
        String[] inputLine = {"4", "1", "20", "15", "21", "-4", "-1", "9", "10", "11", "7"};
        String[] expected = {"-4", "-1", "1", "4", "9", "10", "11", "15", "20", "21"};

        assertArrayEquals(expected, sorting.sort(inputLine));
    }

    @Test
    public void testTenElementArrayCase() {
        String[] inputLine = {"1", "20", "15", "21", "-4", "-1", "9", "10", "11", "7"};
        String[] expected = {"-4", "-1", "1", "7", "9", "10", "11", "15", "20", "21"};

        assertArrayEquals(expected, sorting.sort(inputLine));
    }

    @Test
    public void testSingleElementArrayCase() {
        assertArrayEquals(new String[]{"-4"}, sorting.sort(new String[]{"-4"}));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyArrayCase() {
        sorting.sort(new String[]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullArrayCase() {
        sorting.sort(null);
    }

}
