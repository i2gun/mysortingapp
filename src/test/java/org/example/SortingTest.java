package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class SortingTest {
    @Parameterized.Parameter(0)
    public String[] inputLine;

    @Parameterized.Parameter(1)
    public String[] expected;
    Main sorting = new Main();

    @Parameterized.Parameters(name= "{index}: testSortingApp[{0}]={1}")
    public static Collection testCase() {
        return Arrays.asList(new String[][][] {
                { {"4", "1", "20", "15", "21", "-4", "-1", "9", "10"},
                                     {"-4", "-1", "1", "4", "9", "10", "15", "20", "21"} },
                { {"-1", "-4", "10", "9", "11"}, {"-4", "-1", "9", "10", "11"} },
                { {"4", "10"}, {"4", "10"} },
                { {"-1", "-4"}, {"-4", "-1"} },
                { {"-4"}, {"-4"} }
        });
    }

    @Test
    public void testSortingApp() {
        assertArrayEquals(expected, sorting.sort(inputLine));
    }

}
