package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

/** Maven project for EPAM's training course
 *  Main class implementing quick sort algorithm
 *  and maintaining passable exceptions regarding
 *  input data represented by array of Strings
 */
public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        if (args == null) logger.error("Argument's string array is null");
        if (args.length == 0) logger.error("Argument's string array is empty");
        if (args == null || args.length == 0) throw new IllegalArgumentException();

        Main main = new Main();
        logger.debug("Input array is: " + Arrays.toString(args));
        String[] sortedArray = main.sort(args);
        logger.debug("Sorted array is: " + Arrays.toString(sortedArray));
        System.out.println(Arrays.toString(sortedArray));
        logger.info("Array has been sorted.");
    }

    /**
     * "sort" - is the only public method of the class
     * which is used to sort provided array of integer elements
     * represented by their String values
     * This method calls private method "quickSort"
     *
     * @param args - array of String values that represent an array of integer elements
     * @return - returns new array of String values that represent a sorted array of integer elements
     */
    public String[] sort(String[] args) {
        if (args == null || args.length == 0) throw new IllegalArgumentException();
        int num = 10;
        num = Math.min(num, args.length);

        int[] items = new int[num];
        for (int i = 0; i < num; i++) {
            if (args[i] == null) throw new IllegalArgumentException();
            try {
                items[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException();
            }
        }

        if (num > 2) {
            quickSort(items, 0, num - 1);
        } else if (num == 2 && items[1] < items[0]) {
            int swap = items[0];
            items[0] = items[1];
            items[1] = swap;
        }

        return Arrays.toString(items).split("[\\[\\]]")[1].split(", ");
    }

    /**
     * "quickSort" is a private method that implements quick sort algorithm
     * using recursive call and accessory method "partition"
     * @param arr
     * @param low
     * @param high
     */
    private void quickSort(int arr[], int low, int high) {
        if (low < high) {
            int pi = partition(arr, low, high);

            quickSort(arr, low, pi - 1);
            quickSort(arr, pi + 1, high);
        }
    }

    private int partition(int arr[], int low, int high) {
        int pivot = arr[high];
        int i = (low - 1);

        for (int j = low; j < high; j++) {
            if (arr[j] <= pivot) {
                i++;

                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        int temp = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp;

        return i + 1;
    }

}